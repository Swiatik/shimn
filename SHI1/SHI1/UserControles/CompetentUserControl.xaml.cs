﻿using SHI1.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHI1.UserControles
{
    /// <summary>
    /// Interaction logic for CompetentUserControl.xaml
    /// </summary>
    public partial class CompetentUserControl : UserControl
    {
        public CompetentUserControl()
        {
            InitializeComponent();
        }
        public void Refresh()
        {
            Test11.IsChecked = false;
            Test12.IsChecked = false;
            Test13.IsChecked = false;
            Test21.IsChecked = false;
            Test22.IsChecked = false;
            Test23.IsChecked = false;
            Test31.IsChecked = false;
            Test32.IsChecked = false;
            Test33.IsChecked = false;            
        }
        public int GetResult()
        {
            User user = (User)App.Current.Resources["authUser"];
            authDBEntities db = new authDBEntities();
            List<Test> tests = new List<Test>();
            tests.Add(new Test()
            {
                test_id = db.Test.Max(x => x.test_id) + 1,
                User = db.User.SingleOrDefault(x => x.user_id == user.user_id),
                user_id = user.user_id,
                Level = db.Level.SingleOrDefault(x => x.level_id == 3),
                level_id = 3
            });
            
            if (Test11.IsChecked == true)
            {
                tests.Last().result = 5;
            }
            if (Test12.IsChecked == true)
            {
                tests.Last().result = 3;
            }
            if (Test13.IsChecked == true)
            {
                tests.Last().result = 2;
            }                                    

            tests.Add(new Test()
            {
                test_id = tests.Last().test_id + 1,
                User = db.User.SingleOrDefault(x => x.user_id == user.user_id),
                user_id = user.user_id,
                Level = db.Level.SingleOrDefault(x => x.level_id == 3),
                level_id = 3

            });
            if (Test21.IsChecked == true)
            {
                tests.Last().result = 5;
            }
            if (Test22.IsChecked == true)
            {
                tests.Last().result = 3;
            }
            if (Test23.IsChecked == true)
            {
                tests.Last().result = 2;
            }
            
            tests.Add(new Test()
            {
                test_id = tests.Last().test_id + 1,
                User = db.User.SingleOrDefault(x => x.user_id == user.user_id),
                user_id = user.user_id,
                Level = db.Level.SingleOrDefault(x => x.level_id == 3),
                level_id = 3
            });            
            if (Test31.IsChecked == true)
            {
                tests.Last().result = 5;
            }
            if (Test32.IsChecked == true)
            {
                tests.Last().result = 3;
            }
            if (Test33.IsChecked == true)
            {
                tests.Last().result = 2;
            }

            if (tests.Any(x => x.result != null))
            {
                db.Test.RemoveRange(db.Test.Where(x => x.user_id == user.user_id && x.level_id == 3)); 
                db.Test.AddRange(tests);
                db.SaveChanges();
            }
            return tests.Where(x => x.result != null).Sum(x => (int)x.result);
        }
    }
}
