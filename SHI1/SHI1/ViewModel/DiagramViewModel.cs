﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHI1.ViewModel
{
    class DiagramViewModel
    {
        public DiagramViewModel(List<Database.Test> data)
        {            
            Points = new List<DataPoint>();
            for(int i = 1; i <= 5;  i++)
            {
                int res = (int)data.Where(x => x.level_id == i).Sum(x => x.result);
                Points.Add(new DataPoint(i - 1, res));
            }                        
            Titles = new List<String>()
            {                            
                "Новачок",
                "Початківець",
                "Компетентний",
                "Досвічений",
                "Експерт"             
            };

        }
        public IList<DataPoint> Points { get; private set; }
        public IList<String> Titles { get; private set; }
    }
}
