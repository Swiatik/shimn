﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHI1.ViewModel
{
    class GraphViewModel
    {
        public GraphViewModel(List<Database.Test> data, int max, string tag)
        {
            Tag = tag;
            Title = $"{tag}(сумарна оцінка{data.Sum(x => x.result)})";
            Max = max;
            Points = new List<DataPoint>();
            for (int i = 0; i < data.Count(); i++)
            {                
                int res = (data[i].result == null) ? 0 : (int)data[i].result;
                Points.Add(new DataPoint(i + 1, res));
            }                        
        }

        public string Title { get; private set; }
        public string Tag { get; private set; }
        public int Max { get; private set; }
        public IList<DataPoint> Points { get; private set; }
    }
}
