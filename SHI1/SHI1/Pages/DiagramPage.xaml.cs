﻿using SHI1.Database;
using SHI1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHI1.Pages
{
    /// <summary>
    /// Interaction logic for DiagramPage.xaml
    /// </summary>
    public partial class DiagramPage : Page
    {
        public DiagramPage()
        {
            InitializeComponent();
            authDBEntities db = new authDBEntities();
            User user = (User)App.Current.Resources["authUser"];
            DataContext = new DiagramViewModel(db.Test.Where(x => x.user_id == user.user_id).ToList()); 
        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
            mainWindow.MainFrame.Navigate(new MainMenuPage());
        }
    }
}
