﻿using SHI1.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHI1.Pages
{
    /// <summary>
    /// Interaction logic for AuthPage.xaml
    /// </summary>
    public partial class AuthPage : Page
    {
        public AuthPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
            mainWindow.MainFrame.Navigate(new RegistrationPage());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (LoginBox.Text == "" || PassBox.Password == "")
            {
                MessageBox.Show("Заповніть поле!");
                return;
            }

            authDBEntities db = new authDBEntities();
            User user = null;
            user = db.User.Where(x => x.login == LoginBox.Text.Trim()).FirstOrDefault();            

            if (user == null)
            {
                MessageBox.Show("Неправильний логін!");
            }
            else if (user.password != PassBox.Password.Trim())
            {
                MessageBox.Show("Неправильний пароль!");
            }
            else
            {
                App.Current.Resources.Add("authUser", user);                    
                MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
                MainMenuPage page = new MainMenuPage();                
                mainWindow.MainFrame.Navigate(page);
            }            
        }
    }
}
