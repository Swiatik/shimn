﻿using SHI1.Database;
using SHI1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHI1.Pages
{
    /// <summary>
    /// Interaction logic for QuestPage.xaml
    /// </summary>
    public partial class QuestPage : Page
    {
        public QuestPage()
        {
            InitializeComponent();            
        }
        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {            
            authDBEntities db = new authDBEntities();
            User user = (User)App.Current.Resources["authUser"];
            MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
            QuestPage page = new QuestPage();
            page.Graph.Visibility = Visibility.Visible;


            if (Graph.Visibility == Visibility.Visible)
            {
                MainMenuButton_Click(this, new RoutedEventArgs());
                return;
            }
            else if (Novice.Visibility == Visibility.Visible)
            {
                if (Novice.GetResult() != 0)
                {
                    var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 1).ToList();
                    page.Graph.DataContext = new GraphViewModel(data, 5, "Новачок");
                }
                else
                {
                    page = new QuestPage();
                    page.Novice.Visibility = Visibility.Visible;
                }

            }
            else if (Beginner.Visibility == Visibility.Visible)
            {
                if (Beginner.GetResult() != 0)
                {
                    var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 2).ToList();
                    page.Graph.DataContext = new GraphViewModel(data, 4, "Твердий початківець");
                }
                else
                {
                    page = new QuestPage();
                    page.Beginner.Visibility = Visibility.Visible;
                }
            }
            else if (Competent.Visibility == Visibility.Visible)
            {
                if (Competent.GetResult() != 0)
                {
                    var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 3).ToList();
                    page.Graph.DataContext = new GraphViewModel(data, 4, "Компетентний");
                }
                else
                {
                    page = new QuestPage();
                    page.Competent.Visibility = Visibility.Visible;
                }
             
            }
            else if (Proficient.Visibility == Visibility.Visible)
            {
                if (Proficient.GetResult() != 0)
                {
                    var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 4).ToList();
                    page.Graph.DataContext = new GraphViewModel(data, 4, "Досвічений");
                }
                else
                {
                    page = new QuestPage();
                    page.Proficient.Visibility = Visibility.Visible;
                }
            }
            else if(Expert.Visibility == Visibility.Visible)
            {
                if (Expert.GetResult() != 0)
                {
                    var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 5).ToList();
                    page.Graph.DataContext = new GraphViewModel(data, 4, "Екперт");
                }
                else
                {
                    page = new QuestPage();
                    page.Expert.Visibility = Visibility.Visible;
                }
            }
            mainWindow.MainFrame.Navigate(page);
        }

        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            Graph.Tag = "None";
            MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
            mainWindow.MainFrame.Navigate(new MainMenuPage());
        }

        private void RestartButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
            QuestPage page = new QuestPage();
            if (Novice.Visibility == Visibility.Visible || (string)Graph.Tag == "Новачок")
            {
                page.Novice.Visibility = Visibility.Visible;
                Remove(1);                
            }
            else if (Beginner.Visibility == Visibility.Visible || (string)Graph.Tag == "Твердий початківець")
            {
                page.Beginner.Visibility = Visibility.Visible;
                Remove(2);
            }
            else if (Competent.Visibility == Visibility.Visible || (string)Graph.Tag == "Компетентний")
            {
                page.Competent.Visibility = Visibility.Visible;
                Remove(3);
            }
            else if (Proficient.Visibility == Visibility.Visible || (string)Graph.Tag == "Досвічений")
            {
                page.Proficient.Visibility = Visibility.Visible;
                Remove(4);
            }
            else 
            {
                page.Expert.Visibility = Visibility.Visible;
                Remove(5);                
            }

            mainWindow.MainFrame.Navigate(page);
        }
        
        public void Remove(int level)
        {
            User user = (User)App.Current.Resources["authUser"];
            authDBEntities db = new authDBEntities();
            db.Test.RemoveRange(db.Test.Where(x => x.user_id == user.user_id && x.level_id == level));
            db.SaveChanges();
        }
    }
}
