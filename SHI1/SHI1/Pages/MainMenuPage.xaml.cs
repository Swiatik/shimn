﻿using SHI1.Database;
using SHI1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHI1.Pages
{
    /// <summary>
    /// Interaction logic for MainMenuPage.xaml
    /// </summary>
    public partial class MainMenuPage : Page
    {
        MainWindow mainWindow;
        User user;
        QuestPage page;
        //authDBEntities db;
        public MainMenuPage()
        {
            InitializeComponent();
            user = (User)App.Current.Resources["authUser"];
            mainWindow = (MainWindow)App.Current.MainWindow;
            page = new QuestPage();            
            GreetingsLabel.Content = "Вітаю, " +  user.login + "!";
            authDBEntities db = new authDBEntities();

            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 1))
            {
                NoviceButton.Background = Brushes.LightGreen;
            }
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 2))
            {
                BeginnerButton.Background = Brushes.LightGreen;
            }
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 3))
            {
                CompetentButton.Background = Brushes.LightGreen;
            }
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 4))
            {
                ProficientButton.Background = Brushes.LightGreen;
            }
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 5))
            {
                ExpertButton.Background = Brushes.LightGreen;
            }            
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            authDBEntities db = new authDBEntities();
            if (db.Test.Any(x => x.user_id == user.user_id))
            {
                mainWindow.MainFrame.Navigate(new DiagramPage()
                {
                    DataContext = new DiagramViewModel(db.Test.Where(x => x.user_id == user.user_id).ToList())
                });
            }
            else
            {
                MessageBox.Show("Ви не пройшли жодного тесту!");
            }                                                      
        }

        private void NoviceButton_Click(object sender, RoutedEventArgs e)
        {
            authDBEntities db = new authDBEntities();
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 1))
            {
                page.Graph.Visibility = Visibility.Visible;
                var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 1).ToList();
                page.Graph.DataContext = new GraphViewModel(data, 5, "Новачок");
            }
            else
            {
                page.Novice.Visibility = Visibility.Visible;
            }
            mainWindow.MainFrame.Navigate(page);
        }

        private void BeginnerButton_Click(object sender, RoutedEventArgs e)
        {
            authDBEntities db = new authDBEntities();
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 2))
            {
                page.Graph.Visibility = Visibility.Visible;
                var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 2).ToList();
                page.Graph.DataContext = new GraphViewModel(data, 4, "Твердий початківець");
            }
            else
            {
                page.Beginner.Visibility = Visibility.Visible;
            }
            mainWindow.MainFrame.Navigate(page);
        }

        private void CompetentButton_Click(object sender, RoutedEventArgs e)
        {
            authDBEntities db = new authDBEntities();
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 3))
            {
                page.Graph.Visibility = Visibility.Visible;
                var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 3).ToList();
                page.Graph.DataContext = new GraphViewModel(data, 4, "Компетентний");
            }
            else
            {
                page.Competent.Visibility = Visibility.Visible;
            }
            mainWindow.MainFrame.Navigate(page);
        }

        private void ProficientButton_Click(object sender, RoutedEventArgs e)
        {
            authDBEntities db = new authDBEntities();
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 4))
            {
                page.Graph.Visibility = Visibility.Visible;
                var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 4).ToList();
                page.Graph.DataContext = new GraphViewModel(data, 4, "Досвічений");
            }
            else
            {
                page.Proficient.Visibility = Visibility.Visible;
            }
            mainWindow.MainFrame.Navigate(page);
        }

        private void ExpertButton_Click(object sender, RoutedEventArgs e)
        {
            authDBEntities db = new authDBEntities();
            if (db.Test.Any(x => x.user_id == user.user_id && x.level_id == 5))
            {
                page.Graph.Visibility = Visibility.Visible;
                var data = db.Test.Where(x => x.user_id == user.user_id && x.level_id == 5).ToList();
                page.Graph.DataContext = new GraphViewModel(data, 4, "Експерт");
            }
            else
            {
                page.Expert.Visibility = Visibility.Visible;
            }
            mainWindow.MainFrame.Navigate(page);
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {            
            App.Current.Resources.Remove("authUser");
            mainWindow.MainFrame.Navigate(new AuthPage());
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Ви впевнені, що бажаєте очистити всі результати?", "Message", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                authDBEntities db = new authDBEntities();
                User user = (User)App.Current.Resources["authUser"];
                db.Test.RemoveRange(db.Test.Where(x => x.user_id == user.user_id));
                db.SaveChanges();
                mainWindow.MainFrame.Navigate(new MainMenuPage());
            }

        }
    }
}
