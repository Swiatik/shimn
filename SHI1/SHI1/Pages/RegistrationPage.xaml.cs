﻿using SHI1.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SHI1.Pages
{
    /// <summary>
    /// Interaction logic for RegistrationPage.xaml
    /// </summary>
    public partial class RegistrationPage : Page
    {
        public RegistrationPage()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
            mainWindow.MainFrame.Navigate(new AuthPage());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            authDBEntities db = new authDBEntities();                

            if (LoginBox.Text == "" || PassBox.Password == "" || PassBox2.Password == "")
            {
                MessageBox.Show("Заповніть поле!");
            }
            else if (PassBox.Password != PassBox2.Password)
            {
                MessageBox.Show("Невірно підтверджений пароль!");
            }
            else if (db.User.Any(x => x.login == LoginBox.Text.Trim()))
            {
                MessageBox.Show("Уже існує користувач з таким логіном!");
            }
            else
            {
                User newUser = new User()
                {
                    user_id = db.User.Max(x => x.user_id) + 1,
                    login = LoginBox.Text.Trim(),
                    password = PassBox.Password.Trim()                    
                };               
                db.User.Add(newUser);
                db.SaveChanges();

                App.Current.Resources.Add("authUser", newUser);
                MainWindow mainWindow = (MainWindow)App.Current.MainWindow;
                MainMenuPage page = new MainMenuPage();
                page.GreetingsLabel.Content = "Вітаю, " + newUser.login + "!";              
                mainWindow.MainFrame.Navigate(page);                
            }


        }
    }
}
